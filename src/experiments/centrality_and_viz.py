import numpy as np
from src.extract_cooccurrence_matrix import get_cooccurrence_matrix
from utils.utils_path_generation import get_input_path
import networkx as nx
from networkx.algorithms.centrality import degree_centrality, betweenness_centrality, eigenvector_centrality
import pandas as pd
from networkx.algorithms import community

path = get_input_path('test_cooccurrence_matrix.json')
c_m = get_cooccurrence_matrix(path)
n_c_m = {i: {j: c_m[i][j] for j in c_m[i] if j in ['albertbichot (PERS)', 'matthieumangenot (PERS)', 'simondepaquit (PERS)']} for i in c_m }
index = {i: list(c_m.keys())[i] for i in range(len(list(c_m.keys())))}
M = np.matrix([[0 for i in range(len(list(c_m.keys())))] for j in range(len(list(c_m.keys())))])
for j in index:
    for i in index:
        M[i, j] = n_c_m[index[j]][index[i]]
G = nx.from_numpy_matrix(M)
C_degree = degree_centrality(G)
C_betweenness = betweenness_centrality(G)
C_eigenvector_centrality = eigenvector_centrality(G)

a = pd.read_csv('/home/utilisateur/Documents/Travail/LMP/nlp_at_lmp/indus_influenceurs/to gephi.csv')
a = a.drop('Unnamed: 0', 1)
index = {i: list(a.columns)[i] for i in range(len(list(a.columns)))}
M = a.values
G = nx.from_numpy_matrix(M)
C_degree = degree_centrality(G)
s_1 = sorted(C_degree.items(), key=lambda kv: kv[1])
print('for degree experiments : {} {} {}'.format(index[s_1[69][0]], index[s_1[68][0]], index[s_1[67][0]]))
C_betweenness = betweenness_centrality(G)
s_2 = sorted(C_betweenness.items(), key=lambda kv: kv[1])
print('for betweeness experiments : {} {} {}'.format(index[s_2[69][0]], index[s_2[68][0]], index[s_2[67][0]]))
C_eigenvector_centrality = eigenvector_centrality(G)
s_3 = sorted(C_eigenvector_centrality.items(), key=lambda kv: kv[1])
print('for eigenvector experiments : {} {} {}'.format(index[s_3[69][0]], index[s_3[68][0]], index[s_3[67][0]]))

H = nx.relabel_nodes(G, index)
nx.draw(H, pos=nx.spring_layout(H), with_label=True)

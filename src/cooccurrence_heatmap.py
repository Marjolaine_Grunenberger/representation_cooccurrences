import numpy as np
from bokeh.plotting import figure, show, output_file
from bokeh.models import HoverTool
from src.extract_cooccurrence_matrix import get_cooccurrence_matrix
from utils.utils_path_generation import get_output_path

def cooccurrence_to_bokeh_data(c_m):
    data = {"nodes":[], "links":[]}
    for i, ent_1 in enumerate(c_m):
        data["nodes"].append({"name": ent_1, "group": 1})
        for j, ent_2 in enumerate(c_m):
            if ent_1 == ent_2:
                data["links"].append({"source": i, "target": j, "value": 0})
            else:
                data["links"].append({"source":i, "target":j, "value":c_m[ent_1][ent_2]})
            #data["links"].append({"source":j, "target":i, "value":c_m[ent_2][ent_1]})
    return data


c_m = get_cooccurrence_matrix('test_cooccurrence_matrix.json')

data = cooccurrence_to_bokeh_data(c_m)
nodes = data['nodes']
names = [node['name'] for node in sorted(data['nodes'], key=lambda x: x['group'])]

N = len(nodes)
counts = np.zeros((N, N))
for link in data['links']:
    counts[link['source'], link['target']] = link['value']
    counts[link['target'], link['source']] = link['value']

#colormap = ["#444444", "#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a"]

colormap = ["#a6cee3", "#1f78b4", "#444444"]

xname = []
yname = []
color = []
alpha = []
for i, node1 in enumerate(nodes):
    for j, node2 in enumerate(nodes):
        xname.append(node1['name'])
        yname.append(node2['name'])

        alpha.append(min(counts[i,j]/4.0, 0.9) + 0.1)

        if node1['group'] == node2['group']:
            color.append(colormap[node1['group']])
        else:
            color.append('lightgrey')

data= dict(
    xname=xname,
    yname=yname,
    colors=color,
    alphas=alpha,
    count=counts.flatten(),
)

data =  {'xname': xname, 'yname': yname, 'colors': color, 'alphas': alpha, 'count': counts.flatten()}

hover = HoverTool()
hover.tooltips = [('names', '@yname, @xname'), ('count', '@count')]

p = figure(title="Co-occurrence matrix",
           x_axis_location="above", tools="hover,save",
           x_range=list(reversed(names)), y_range=names
           )

p.tools.append(hover)

p.plot_width = 800
p.plot_height = 800
p.grid.grid_line_color = None
p.axis.axis_line_color = None
p.axis.major_tick_line_color = None
p.axis.major_label_text_font_size = "5pt"
p.axis.major_label_standoff = 0
p.xaxis.major_label_orientation = np.pi/3

p.rect('xname', 'yname', 0.9, 0.9, source=data,
       color='colors', alpha='alphas', line_color=None,
       hover_line_color='black', hover_color='colors')

output_file(get_output_path("cooccurrence_matrix.html"), title="co-occurrence matrix")

show(p)

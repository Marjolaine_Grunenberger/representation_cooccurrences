import json

def get_cooccurrence_matrix(filename):
    with open(filename, 'r') as f:
        data = json.load(f)
    return {list(data[i].keys())[0]: data[i][list(data[i].keys())[0]] for i in range(len(data))}

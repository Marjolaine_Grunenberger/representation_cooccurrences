from src.extract_cooccurrence_matrix import get_cooccurrence_matrix
import copy
from math import log


def map_c_m(c_m, mapping='binary'):
    c_m_2 = copy.deepcopy(c_m)
    for ent_1 in c_m:
        count = 0
        for ent_2 in c_m[ent_1]:
            count += c_m[ent_1][ent_2]
            if c_m[ent_1][ent_2] != 0:
                if mapping == 'binary':
                    c_m_2[ent_1][ent_2] = 1
                if mapping == 'log':
                    c_m_2[ent_1][ent_2] = log(c_m[ent_1][ent_2])
            if mapping == 'normalise' and count != 0:
                c_m_2[ent_1] = {i: c_m[ent_1][i]/count for i in c_m[ent_1]}
    return c_m_2



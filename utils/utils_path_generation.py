from config.global_paths import input_path, output_path,  PROJECT_PATH
import os

def get_input_path(filename):
    return os.path.join(PROJECT_PATH, input_path, filename)

def get_output_path(filename):
    return os.path.join(PROJECT_PATH, output_path, filename)